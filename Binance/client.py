import sys
import time
sys.path.append("..")

from endpoints import *
from binance_f.requestclient import RequestClient
from binance_f.model.constant import *


class Client:
    def __init__(self):
        self.client = RequestClient(
            api_key=API_KEY,
            secret_key=SECRET_KEY,
            url=BASE
        )

        self.market = None
        self.first_take = None
        self.second_take = None
        self.stop = None

        self.active_position = None
        self.signal = {}

        try:
            self.client.change_initial_leverage(symbol=SYMBOL, leverage=LEVERAGE)
            self.client.change_margin_type(symbol=SYMBOL, marginType=FuturesMarginType.ISOLATED)
        except Exception as e:
            print(e)

        self.balance = self.client.get_balance()[0].balance

    def update_balance(self):
        self.balance = self.client.get_balance()[0].balance

    def update_position(self):
        self.active_position = self.client.get_position()

    def validator(self):
        self.update_position()
        for position in self.active_position:
            if position.positionAmt:
                return False

        try:
            self.client.cancel_all_orders(symbol=SYMBOL)
        except Exception as e:
            print(e)

        self.update_balance()

        return True

    def trade(self, signal):
        if self.validator():
            self.signal = signal
            if signal['trade'] == "long":
                # MARKET_LONG
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.MARKET,
                    quantity=self.signal['amount'],
                    newClientOrderId="enter"
                )
                time.sleep(4)
                self.long_oreders()
                return True

            if signal['trade'] == "short":
                #MARKET_SHORT
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.MARKET,
                    quantity=self.signal['amount'],
                    newClientOrderId="market"
                )
                time.sleep(2)
                self.short_oreders()
                return True
            return False

    def get_enter_price(self):
        self.update_position()
        price = 0
        for position in self.active_position:
            if position.positionAmt:
                price = position.entryPrice
                return price

    def get_precisions(self):
        precision = 4
        if self.signal["trade"] == "long":
            enter = self.get_enter_price()
            stop = enter * (1 - float(self.signal["stop"]))
            take = enter * (1 + float(self.signal["take"]))

            # self.signal["first_take_price"] = "{price:0.{precision}f}".format(price=first_take, precision=precision)
            # self.signal["second_take_price"] = "{price:0.{precision}f}".format(price=second_take, precision=precision)
            self.signal["stop"] = "{price:0.{precision}f}".format(price=stop, precision=precision)
            self.signal["take"] = "{price:0.{precision}f}".format(price=take, precision=precision)
            self.signal["enter"] = "{price:0.{precision}f}".format(price=enter*1.002, precision=precision)
        elif self.signal["trade"] == "short":
            enter = self.get_enter_price()
            stop = enter + self.signal["atr"]*self.signal["stop"]
            # first_take = enter * (1 - self.signal["first_take"])
            # second_take = enter * (1 - self.signal["second_take"])

            # self.signal["first_take_price"] = "{price:0.{precision}f}".format(price=first_take, precision=precision)
            # self.signal["second_take_price"] = "{price:0.{precision}f}".format(price=second_take, precision=precision)
            self.signal["stop_price"] = "{price:0.{precision}f}".format(price=stop, precision=precision)
            self.signal["enter"] = "{price:0.{precision}f}".format(price=enter*0.998, precision=precision)

    def long_oreders(self):
        # FIRST_TAKE_LONG
        self.get_precisions()

        # self.client.post_order(
        #     symbol=SYMBOL,
        #     side=OrderSide.SELL,
        #     ordertype=OrderType.TAKE_PROFIT_MARKET,
        #     stopPrice=self.signal['first_take_price'],
        #     quantity=self.signal['first_take_amount'],
        #     newClientOrderId="first"
        # )
        # # SECOND_TAKE_LONG
        # self.client.post_order(
        #     symbol=SYMBOL,
        #     side=OrderSide.SELL,
        #     ordertype=OrderType.TAKE_PROFIT_MARKET,
        #     stopPrice=self.signal['second_take_price'],
        #     closePosition=True,
        #     newClientOrderId="second"
        # )

        # STOP_LONG
        self.client.post_order(
            symbol=SYMBOL,
            side=OrderSide.SELL,
            ordertype=OrderType.STOP_MARKET,
            stopPrice=self.signal['stop_price'],
            newClientOrderId="stop",
            closePosition=True
        )

    def short_oreders(self):
        self.get_precisions()
        # FIRST_TAKE_SHORT
        # self.client.post_order(
        #     symbol=SYMBOL,
        #     side=OrderSide.BUY,
        #     ordertype=OrderType.TAKE_PROFIT_MARKET,
        #     stopPrice=self.signal['first_take_price'],
        #     quantity=self.signal['first_take_amount'],
        #     newClientOrderId="first"
        # )
        # # SECOND_TAKE_SHORT
        # self.client.post_order(
        #     symbol=SYMBOL,
        #     side=OrderSide.BUY,
        #     ordertype=OrderType.TAKE_PROFIT_MARKET,
        #     stopPrice=self.signal['second_take_price'],
        #     closePosition=True,
        #     newClientOrderId="second"
        # )

        # STOP_SHORT
        self.client.post_order(
            symbol=SYMBOL,
            side=OrderSide.BUY,
            ordertype=OrderType.STOP_MARKET,
            stopPrice=self.signal['stop_price'],
            newClientOrderId="stop",
            closePosition=True
        )

    def rebase_stop(self, signal):
        if not self.validator():
            try:
                if signal['trade'] == 'long':
                    # STOP_LONG
                    self.client.post_order(
                        symbol=SYMBOL,
                        side=OrderSide.SELL,
                        ordertype=OrderType.STOP_MARKET,
                        stopPrice=self.signal['enter'],
                        newClientOrderId="new_stop",
                        closePosition=True
                    )
                elif signal['trade'] == 'short':
                    # STOP_LONG
                    self.client.post_order(
                        symbol=SYMBOL,
                        side=OrderSide.BUY,
                        ordertype=OrderType.STOP_MARKET,
                        stopPrice=self.signal['enter'],
                        newClientOrderId="new_stop",
                        closePosition=True
                    )
                self.client.cancel_order(symbol=SYMBOL, origClientOrderId="stop")
            except Exception:
                return False

            return True

    def close_active_position(self, trade, ak_trend, amount):
        if not self.validator():
            if trade == 'long' and ak_trend < 0:
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.MARKET,
                    quantity=amount,
                    newClientOrderId="close"
                )
            elif trade == 'short' and ak_trend > 0:
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.MARKET,
                    quantity=amount,
                    newClientOrderId="close"
                )
            else:
                return False
            try:
                self.client.cancel_order(symbol=SYMBOL, origClientOrderId="stop")
            except Exception:
                pass
        return True


if __name__ == "__main__":
    client = Client()
    import ipdb
    ipdb.set_trace()