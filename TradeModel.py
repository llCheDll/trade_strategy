import endpoints

from analyzer import Analyzer
from datetime import datetime
from data_collector import DataCollector


class TradeModel:
    def __init__(self):
        # self.data = DataCollector().history
        self.data = Analyzer()
        self.data.data.get_old_hist()
        self.trade_history = []
        self.current_trade = {}
        self.take_percent = endpoints.TAKE
        self.stop_percent = endpoints.STOP

        self.counter_orders = {"first_take": 0, "second_take": 0, "stop": 0}

        self.deposit = 839
        self.trade_quantity = 0.1
        self.value = 10

        self.flag = 0
        self.prev_price = 0

    def trade(self):
        for row in self.data.data.history.iterrows():
            if self.current_trade:
                if self.get_results_def(row):
                    self.trade_history.append(self.current_trade)
                    self.current_trade = 0
            if not self.current_trade:
                self.current_trade = self.data.get_signal_hist(row)
                if self.current_trade:
                    self.stop_calculate(self.current_trade)

            # elif self.get_stop_bu(row):

        return self.trade_history

    def stop_calculate(self, trade):
        amount = self.deposit*self.trade_quantity/trade['enter']*self.value

        if trade['trade'] == 'long':
            stop = (endpoints.STOP-trade['enter']*amount)/amount * -1
            self.current_trade['stop'] = stop
        elif trade['trade'] == 'short':
            stop = (endpoints.STOP+trade['enter']*amount)/amount
            self.current_trade['stop'] = stop



    def get_results_default(self, kline):
        #Get result: BUY
        if self.current_trade['trade'] == 'long':
            #take profit
            if kline[1].high >= self.current_trade['take']:
                self.current_trade['close'] = self.current_trade['take']
                self.current_trade['time_close'] = kline[1].time
                self.current_trade['type'] = "by_take"
                return True
            #stop loss
            elif kline[1].low <= self.current_trade['stop']:
                self.current_trade['close'] = self.current_trade['stop']
                self.current_trade['time_close'] = kline[1].time
                self.current_trade['type'] = "by_stop"
                return True
            return False

        #Get results: SELL
        if self.current_trade['trade'] == 'short':
            #take profit
            if kline[1].low <= self.current_trade['take']:
                self.current_trade['close'] = self.current_trade['take']
                self.current_trade['time_close'] = kline[1].time
                self.current_trade['type'] = "by_take"
                return True
            #stop loss
            elif kline[1].high >= self.current_trade['stop']:
                self.current_trade['close'] = self.current_trade['stop']
                self.current_trade['time_close'] = kline[1].time
                self.current_trade['type'] = "by_stop"
                return True
            return False

        #Print result

    def get_stop_bu(self, kline):
        #Get result: BUY
        self.current_trade['trade_value'] = None
        if self.current_trade['trade'] == 'long':
            if kline[1].low <= self.current_trade['stop']:
                if self.current_trade['Flag']:
                    self.current_trade['close'] = self.current_trade['take2']
                    self.current_trade['trade_value'] = endpoints.TRADE_VALUE2
                    self.current_trade['type'] = "by_FIRST_TAKE"
                    self.counter_orders['first_take'] += 1
                else:
                    self.current_trade['close'] = self.current_trade['stop']
                    self.current_trade['trade_value'] = 1
                    self.counter_orders['stop'] += 1
                    self.current_trade['type'] = "by_STOP"
                self.current_trade['time_close'] = kline[1].time
                return True
            elif kline[1].high >= self.current_trade['take2'] and self.current_trade['Flag'] == 0:
                self.current_trade['Flag'] = 1
                self.current_trade['stop'] = self.current_trade['enter']
            elif kline[1].high >= self.current_trade['take']:
                self.current_trade['close'] = self.current_trade['take2']*endpoints.TRADE_VALUE2 + self.current_trade['take']*endpoints.TRADE_VALUE1
                self.current_trade['trade_value'] = 1
                self.current_trade['time_close'] = kline[1].time
                self.counter_orders['second_take'] += 1
                self.flag = 1
                self.current_trade['type'] = "by_SECOND_TAKE"
                return True
            return False
        # Get result: SELL
        if self.current_trade['trade'] == 'short':
            if kline[1].high >= self.current_trade['stop']:
                if self.current_trade['Flag']:
                    self.current_trade['close'] = self.current_trade['take2']
                    self.current_trade['trade_value'] = endpoints.TRADE_VALUE2
                    self.current_trade['type'] = "by_FIRST_TAKE"
                    self.counter_orders['first_take'] += 1
                else:
                    self.current_trade['close'] = self.current_trade['stop']
                    self.current_trade['trade_value'] = 1
                    self.counter_orders['stop'] += 1
                    self.current_trade['type'] = "by_STOP"
                self.current_trade['time_close'] = kline[1].time
                return True
            elif kline[1].low <= self.current_trade['take2'] and self.current_trade['Flag'] == 0:
                self.current_trade['Flag'] = 1
                self.current_trade['stop'] = self.current_trade['enter']
            elif kline[1].low <= self.current_trade['take']:
                self.current_trade['close'] = self.current_trade['take2']*endpoints.TRADE_VALUE2 + self.current_trade['take']*endpoints.TRADE_VALUE1
                self.current_trade['trade_value'] = 1
                self.current_trade['time_close'] = kline[1].time
                self.counter_orders['second_take'] += 1
                self.current_trade['type'] = "by_SECOND_TAKE"
                self.flag = 1
                return True
            return False

    def get_results_def(self, kline):
        #Get result: BUY
        if self.current_trade['trade'] == 'long':
            close_price = 0
            if self.current_trade['stop'] >= kline[1].low:
                close_price = self.current_trade['stop']
            elif kline[1].low <= self.current_trade['take'] <= kline[1].high:
                close_price = self.current_trade['take']
            elif kline[1].FAST < kline[1].SLOW:
                close_price = kline[1].close
            else:
                return False
            self.current_trade['close'] = close_price
            self.current_trade['time_close'] = kline[1].time
            self.current_trade['trade_value'] = endpoints.TRADE_VALUE2
            if self.current_trade['enter'] < self.current_trade['close']:
                self.current_trade['type'] = "by_TAKE"
                self.counter_orders['second_take'] += 1
            else:
                self.current_trade['type'] = "by_STOP"
                self.counter_orders['stop'] += 1
            return True
        # Get result: SELL
        if self.current_trade['trade'] == 'short':
            close_price = 0
            if self.current_trade['stop'] <= kline[1].high:
                close_price = self.current_trade['stop']
            elif kline[1].high >= self.current_trade['take'] >= kline[1].low:
                close_price = self.current_trade['take']
            elif kline[1].FAST > kline[1].SLOW:
                close_price = kline[1].close
            else:
                return False
            self.current_trade['close'] = close_price
            self.current_trade['time_close'] = kline[1].time
            self.current_trade['trade_value'] = endpoints.TRADE_VALUE2
            if self.current_trade['enter'] > self.current_trade['close']:
                self.current_trade['type'] = "by_TAKE"
                self.counter_orders['second_take'] += 1
            else:
                self.current_trade['type'] = "by_STOP"
                self.counter_orders['stop'] += 1
            return True

    def get_statistics(self):
        print(f'Graph: {endpoints.SYMBOL}; Timeframe: {endpoints.INTERVAL}; Limit: {endpoints.LIMIT}'
              f'Period: {endpoints.PRICE_CHANNEL_PERIOD}; Take:{endpoints.TAKE}, Stop:{endpoints.STOP}')
        for trade in self.trade_history:
            if trade['trade'] == 'long':
                trade_value = self.deposit*self.trade_quantity/trade['enter']*self.value
                trade['profit_dollars'] = (trade['close']*trade_value - trade['enter']*trade_value)*trade['trade_value']
                trade['profit_percent'] = trade['profit_dollars']*100/self.deposit
                self.deposit += trade['profit_dollars']
                trade['deposite'] = self.deposit
                trade['time'] = datetime.fromtimestamp(trade['time']/1000)
                trade['time_close'] = datetime.fromtimestamp(trade['time_close']/1000)

            if trade['trade'] == 'short':
                trade_value = self.deposit*self.trade_quantity/trade['enter']*self.value
                trade['profit_dollars'] = (trade['enter'] * trade_value - trade['close'] * trade_value)*trade['trade_value']
                trade['profit_percent'] = trade['profit_dollars'] * 100 / self.deposit
                self.deposit += trade['profit_dollars']
                trade['deposite'] = self.deposit
                trade['time'] = datetime.fromtimestamp(trade['time'] / 1000)
                trade['time_close'] = datetime.fromtimestamp(trade['time_close'] / 1000)

        for trade in self.trade_history:
            print(
                str(trade['time']) +
                "; Tclose: " +
                str(trade['time_close']) +
                ";"+str(trade['deposite'])+
                "; Profit="+"{price:0.{precision}f}".format(
                    price=trade['profit_dollars'], precision=6
                ) +
                "; E:" + str(trade["enter"]) +
                "; C:" + str(trade["close"]) +
                "; TR:" + str(trade["trade"]) +
                "; TYPE:" + str(trade["type"])
            )
        print(self.current_trade)
        print(
            "SECOND_TAKE:"+str(
                self.counter_orders['second_take']
            )+"; FIRST_TAKE"+str(
                self.counter_orders['first_take']
            )+"; STOP"+str(
                self.counter_orders['stop']
            )
        )


if __name__ == '__main__':
    model = TradeModel()
    model.trade()
    model.get_statistics()
