import endpoints
import pandas as pd
import requests
import json

from datetime import datetime


class DataCollector:
    def __init__(self):
        self.period_pc = endpoints.PRICE_CHANNEL_PERIOD
        self.period_stoch = endpoints.STOCH_PERIOD
        self.df = None
        self.history = pd.DataFrame()

    def get_data(self):
        url = endpoints.BASE + \
              endpoints.KLINES + \
              endpoints.KLINE_HISTORY_PARAM

        data = requests.get(url)
        dictionary = json.loads(data.text)

        column_names = ["time", "open", "high", "low", "close", "volume", "time_close"]
        self.df = pd.DataFrame.from_dict(dictionary)
        self.df.drop(columns=range(7, 12), inplace=True)
        self.df.columns = column_names

        #Clear data
        for column in column_names[1:]:
            self.df[column] = self.df[column].astype(float)

        # self.df = self.df[:endpoints.LIMIT - 1]
        self.df = self.df

        self.add_ema(endpoints.EMA)
        self.add_ema(endpoints.EMA2)
        self.add_atr(endpoints.ATR)

    def get_data_history(self, time_start):
        url = endpoints.BASE + \
              endpoints.KLINES + \
              endpoints.KLINE_HISTORY_PARAM_BIG_DATA.format(
                  endpoints.SYMBOL,
                  endpoints.INTERVAL,
                  time_start,
                  endpoints.LIMIT
              )

        data = requests.get(url)
        dictionary = json.loads(data.text)

        hist = pd.DataFrame.from_dict(dictionary)
        self.history = self.history.append(other=hist[1:], ignore_index=True)

    def get_old_hist(self):
        counter = 20
        dt_obj = datetime.strptime('20.11.2020 00:00:00,00',
                                   '%d.%m.%Y %H:%M:%S,%f')
        millisec = int(dt_obj.timestamp() * 1000)

        for i in range(0, counter):
            if self.history.empty:
                self.get_data_history(millisec)
            self.get_data_history(self.history[0][len(self.history)-1])

        column_names = ["time", "open", "high", "low", "close", "volume", "time_close"]
        self.history.drop(columns=range(7, 12), inplace=True)
        self.history.columns = column_names

        #Clear data
        for column in column_names[1:]:
            self.history[column] = self.history[column].astype(float)

        self.add_price_channel_hist()
        self.add_stoch_hist()
        self.add_ma_hist(endpoints.MA)
        self.add_ma_hist(endpoints.MA2)
        self.add_ema_fast_hist(endpoints.EMA)
        self.add_ema_fast_slow_hist(endpoints.EMASLOW)
        self.add_ema_slow_slow_hist(endpoints.EMA2SLOW)
        self.add_ema_slow_hist(endpoints.EMA2)
        self.atr_hist(endpoints.ATR)
        print(self.history)

    def get_last_closed_kline(self):
        return self.df[0:-1]

    def get_current_price(self):
        url = endpoints.BASE + endpoints.CURRENT_PRICE

        data = requests.get(url)
        dictionary = json.loads(data.text)

        return dictionary

    #indicators
    def add_price_channel(self):
        self.df['pc_upper'] = self.df['high'].rolling(window=self.period_pc).max()
        self.df['pc_lower'] = self.df['low'].rolling(window=self.period_pc).min()

    def add_stoch(self):
        # Create the "L14" column in the DataFrame
        self.df['L'] = self.df['low'].rolling(window=self.period_stoch).min()

        # Create the "H14" column in the DataFrame
        self.df['H'] = self.df['high'].rolling(window=self.period_stoch).max()

        # Create the "%K" column in the DataFrame
        self.df['K'] = 100 * ((self.df['close'] - self.df['L']) / (self.df['H'] - self.df['L']))

        # Create the "%D" column in the DataFrame
        self.df['D'] = self.df['K'].rolling(window=3).mean()

    def add_ma(self, period):
        ma = "MA" + str(period)
        self.df[ma] = self.df['close'].rolling(period).mean()

    def add_ema(self, period):
        ema = "EMA" + str(period)
        self.df[ema] = self.df['close'].ewm(span=period, min_periods=0, adjust=False, ignore_na=False).mean()

    def add_atr(self, period):
        ind = range(0, len(self.df))
        index_list = list(ind)
        self.df.index = index_list

        true_range = []

        for index, row in self.df.iterrows():
            max_tr = 0
            if index != 0:
                tr1 = row['high'] - row['low']
                tr2 = abs(row['high'] - self.df.iloc[index-1]['close'])
                tr3 = abs(row['high'] - self.df.iloc[index-1]['close'])
                max_tr = max(tr1, tr2, tr3)
            true_range.append(max_tr)

        self.df["tr"] = true_range
        self.df["ATR"] = self.df["tr"].rolling(
            min_periods=period, window=period, center=False
        ).mean()

    def add_price_channel_hist(self):
        self.history['pc_upper'] = self.history['high'].rolling(window=self.period_pc).max()
        self.history['pc_lower'] = self.history['low'].rolling(window=self.period_pc).min()

    def add_stoch_hist(self):
        # Create the "L14" column in the DataFrame
        self.history['L'] = self.history['low'].rolling(window=self.period_stoch).min()

        # Create the "H14" column in the DataFrame
        self.history['H'] = self.history['high'].rolling(window=self.period_stoch).max()

        # Create the "%K" column in the DataFrame
        self.history['K'] = 100 * ((self.history['close'] - self.history['L']) / (self.history['H'] - self.history['L']))

        # Create the "%D" column in the DataFrame
        self.history['D'] = self.history['K'].rolling(window=3).mean()

    def add_ma_hist(self, period):
        ma = "MA" + str(period)
        self.history[ma] = self.history['close'].rolling(period).mean()

    def add_ema_fast_hist(self, period):
        self.history["FAST"] = self.history['close'].ewm(span=period, min_periods=0, adjust=False, ignore_na=False).mean()

    def add_ema_slow_hist(self, period):
        self.history["SLOW"] = self.history['close'].ewm(span=period, min_periods=0, adjust=False, ignore_na=False).mean()

    def add_ema_fast_slow_hist(self, period):
        self.history["FASTSLOW"] = self.history['close'].ewm(span=period, min_periods=0, adjust=False, ignore_na=False).mean()

    def add_ema_slow_slow_hist(self, period):
        self.history["SLOWSLOW"] = self.history['close'].ewm(span=period, min_periods=0, adjust=False, ignore_na=False).mean()

    def atr_hist(self, period):
        ind = range(0, len(self.history))
        indexList = list(ind)
        self.history.index = indexList

        true_range = []

        for index, row in self.history.iterrows():
            max_tr = 0
            if index != 0:
                tr1 = row['high'] - row['low']
                tr2 = abs(row['high'] - self.history.iloc[index-1]['close'])
                tr3 = abs(row['high'] - self.history.iloc[index-1]['close'])
                max_tr = max(tr1, tr2, tr3)
            true_range.append(max_tr)

        self.history["tr"] = true_range
        self.history["ATR"] = self.history["tr"].rolling(
            min_periods=period, window=period, center=False
        ).mean()

if __name__ == '__main__':
    dt = DataCollector()
    dt.get_old_hist()
