class PriceChennel:
    """ Стратегия Price Channel.
    Возможные периоды:8, 10, 20, 24, 55, 100
    
        1.Вход в рынок:
           1.1 После пробития границы.
               (1) На открытии следующей свечки.
               (2) Ставим Stop loss и Take profit
           1.2 На откате.
               (1) Ударили верхнюю или нижнюю => 2.1(2,3)
        2.Stop loss:
           2.1 Если сработал:
              (1) Сделка закрывается.
              (2) Дожидаемся разворота в нашу сторону
                  (Индикатор стохастик: Лонг - пересечение снизу вверх;
                                        Шорт - пересечение сверху вниз)
              (3) Смотрим на какой свечке это произошло. => 1.1 (1,2)
        3.Take profit:
           3.1 По Trailing Stop:
              (1)Лонг - подтягиваем стоп по изменению нижней границы. Шорт - верхней.
           3.2 При пробитии противоположной границы.
           3.3 При развороте
                 (Индикатор стохастик: Лонг - пересечение сверху вниз;
                            Шорт - пересечение снизу вверх)
           3.4 При достижении определенного ТР
           3.5 Через орепределенный период
    """
    def __init__(self, df, period):
        self.df = df
        self.period = period
        self.upper_channel()
        self.lower_channel()

    def upper_channel(self):
        # channel = []
        # high = 0
        # counter = 0
        # highs = self.df.get('high')
        #
        # for value in highs:
        #     if value >= high:
        #         high = value
        #         counter = 0
        #     elif counter >= self.period:
        #         index = len(channel) - self.period + 1
        #         high = max(highs[index:len(channel)+1])
        #     channel.append(high)
        #     counter += 1
        #
        # import ipdb
        # ipdb.set_trace()

        self.df['pc_upper'] = self.df['high'].rolling(window=self.period).max()

    def lower_channel(self):
        # channel = []
        # lows = self.df.get('low')
        # low = lows[0]
        # counter = 0
        #
        # for value in lows:
        #     if value <= low:
        #         low = value
        #         counter = 0
        #     elif counter >= self.period:
        #         index = len(channel) - self.period + 1
        #         low = min(lows[index:len(channel)])
        #     channel.append(low)
        #     counter += 1
        self.df['pc_lower'] = self.df['low'].rolling(window=self.period).min()

    def get_signal(self, row, take, stop):
        index = row[0]
        data = row[1]

        #long position
        if data.high == data.pc_upper:
            signal = {
                "index": index,
                "time": data.time,
                "enter": data.close,
                "stop": data.close*(1-stop),
                "take": data.close*(1+take),
                "trade": 'long',
                "K": data.K,
                "D": data.D
            }
        #short position
        elif data.low == data.pc_lower:
            signal = {
                "index": index,
                "time": data.time,
                "enter": data.close,
                "stop": data.close*(1+stop),
                "take": data.close*(1-take),
                "trade": 'short',
                "K": data.K,
                "D": data.D
            }
        else:
            signal = None
            
        return signal
