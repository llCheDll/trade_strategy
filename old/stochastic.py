class Stoch:
    def __init__(self, df, period):
        self.df = df
        self.period = period
        self.get_parameters()

    def get_parameters(self):
        # Create the "L14" column in the DataFrame
        self.df['L'] = self.df['low'].rolling(window=self.period).min()

        # Create the "H14" column in the DataFrame
        self.df['H'] = self.df['high'].rolling(window=self.period).max()

        # Create the "%K" column in the DataFrame
        self.df['K'] = 100 * ((self.df['close'] - self.df['L']) / (self.df['H'] - self.df['L']))

        # Create the "%D" column in the DataFrame
        self.df['D'] = self.df['K'].rolling(window=3).mean()
