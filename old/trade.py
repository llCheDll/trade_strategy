import endpoints
import pandas as pd
import plotly.graph_objs as go
import requests
import json

from analyzer import Analyzer
from plotly.offline import plot
from plotly.subplots import make_subplots
from pc import PriceChennel


class TradeModel:
    def __init__(self, symbol, interval):
        self.data = Analyzer()
        self.trade = None

    def get_results(self, kline):
        #Get result: BUY
        if self.trade['trade'] == 'long':
            #take profit
            if kline[1].high >= self.trade['take']:
                self.trade['close'] = self.trade['take']
                self.trade['time_close'] = kline[1].time
                self.trade['type'] = "by_take"
                return True
            #stop loss
            elif kline[1].low <= self.trade['stop']:
                self.trade['close'] = self.trade['stop']
                self.trade['time_close'] = kline[1].time
                self.trade['type'] = "by_stop"
                return True
            return False

        #Get results: SELL
        if self.trade['trade'] == 'short':
            #take profit
            if kline[1].low <= self.trade['take']:
                self.trade['close'] = self.trade['take']
                self.trade['time_close'] = kline[1].time
                self.trade['type'] = "by_take"
                return True
            #stop loss
            elif kline[1].high >= self.trade['stop']:
                self.trade['close'] = self.trade['stop']
                self.trade['time_close'] = kline[1].time
                self.trade['type'] = "by_stop"
                return True
            return False

        #Print result

        print(
            "Buy orders={}; Take profit={}; Stop loss={};".format(len(self.buy_signal),len(buy_result['take']),len(buy_result['stop']))
        )
        print(
            "Sell orders={}; Take profit={}; Stop loss={};".format(len(self.sell_signal),len(sell_result['take']),len(sell_result['stop']))
        )

    def plot_data(self):
        df = self.df
        quantity_trades = len(self.trades)
        
        take = 0
        stop = 0
        
        take_points = 0
        stop_points = 0

        for trade in self.trades:
            if trade['type'] == "by_take":
                take += 1
                points = trade['close'] - trade['enter']
                if points < 0:
                    points = points*(-1)
                take_points += points
            if trade['type'] == "by_stop":
                stop += 1
                points = trade['close'] - trade['enter']
                if points < 0:
                    points = points*(-1)
                stop_points += points
        
        print(
            "Orders={}; Take ={}; Take_points ={}; Stop={}; Stop_points={};".format(quantity_trades, take, take_points, stop, stop_points)
        )

        kline = go.Candlestick(
            name="Candlestick",
            x=df['time'],
            open=df["open"],
            high=df["high"],
            low=df["low"],
            close=df["close"]
        )

        pc_upper = go.Scatter(
            x=df['time'],
            y=df['pc_upper'],
            line_shape='linear',
            name="Price Channel(Upper)"
        )

        pc_lower = go.Scatter(
            x=df['time'],
            y=df['pc_lower'],
            line_shape='linear',
            name="Price Channel(Lower)"
        )

        order_open = go.Scatter(
            x=[signal['time'] for signal in self.trades],
            y=[signal['enter'] for signal in self.trades],
            mode = 'markers',
            name="Order_open",
            marker=dict(color="Green"),
            hovertext=[
                "Position: {}. Index: {}. Type close: {}".format(trade['trade'], trade['index'], trade['type']) for trade in self.trades
            ]
        )
        
        order_close = go.Scatter(
            x=[signal['time_close'] for signal in self.trades],
            y=[signal['close'] for signal in self.trades],
            mode = 'markers',
            name="Order_close",
            marker=dict(color="Red"),
            hovertext=[
                "Position close: {}. Index: {}".format(signal['trade'], signal['index'], signal['type']) for signal in self.trades
            ]
        )

        osscilator_k = go.Scatter(x=df['time'], y=['%K'])
        osscilator_d = go.Scatter(x=df['time'], y=['%D'])


        
        # data = [kline, pc_upper, pc_lower, order_open, order_close]
        layout = go.Layout(title="Symbol:{}. Time frame:{}".format(self.symbol, self.interval))
        # fig = go.Figure(data=data, layout=layout)

        print(df[['%K','%D']])
        fig = make_subplots(rows=2, cols=1)
        fig.append_trace(kline, row=1, col=1)
        fig.append_trace(pc_upper, row=1, col=1)
        fig.append_trace(pc_lower, row=1, col=1)
        fig.append_trace(order_open, row=1, col=1)
        fig.append_trace(order_close, row=1, col=1)
        fig.append_trace(osscilator_k, row=2, col=1)
        fig.append_trace(osscilator_d, row=2, col=1)

        plot(fig, filename=self.symbol)


if __name__ == '__main__':
    model = TradeModel("BTCUSDT", "1h")
    model.plot_data()
