import plotly.graph_objs as go

from plotly.offline import plot
from plotly.subplots import make_subplots
from endpoints import SYMBOL, INTERVAL
from TradeModel import TradeModel


class Plotter:
    def __init__(self):
        self.trade_model = TradeModel()
        self.df = self.trade_model.data.data.history
        self.trades = self.trade_model.trade()
        self.data = []
        self.fig = make_subplots(rows=2, cols=1)

    def get_klines(self):
        klines = go.Candlestick(
            name="Candlestick",
            x=self.df['time'],
            open=self.df["open"],
            high=self.df["high"],
            low=self.df["low"],
            close=self.df["close"]
        )
        self.fig.append_trace(klines, row=1, col=1)

    def get_price_channel(self):
        pc_upper = go.Scatter(
            x=self.df['time'],
            y=self.df['pc_upper'],
            line_shape='linear',
            name="Price Channel(Upper)"
        )

        pc_lower = go.Scatter(
            x=self.df['time'],
            y=self.df['pc_lower'],
            line_shape='linear',
            name="Price Channel(Lower)"
        )
        self.fig.append_trace(pc_upper, row=1, col=1)
        self.fig.append_trace(pc_lower, row=1, col=1)

    def get_stoch(self):
        k = go.Scatter(x=self.df['time'], y=self.df['K'], name="%K")
        d = go.Scatter(x=self.df['time'], y=self.df['D'], name="%D")

        self.fig.append_trace(k, row=2, col=1)
        self.fig.append_trace(d, row=2, col=1)

    def get_signal(self):
        order_open = go.Scatter(
            x=[signal['time'] for signal in self.trades],
            y=[signal['enter'] for signal in self.trades],
            mode='markers',
            name="Order_open",
            marker=dict(color="Green"),
            # hovertext=[
            #     "Position: {}. Index: {}. Type close: {}".format(trade['trade'], trade['index'], trade['type']) for
            #     trade in self.trades
            # ]
            hovertext=[
                "Position: {}. Index: {}.".format(trade['trade'], trade['index']) for
                trade in self.trades
            ]
        )

        order_close = go.Scatter(
            x=[signal['time_close'] for signal in self.trades],
            y=[signal['close'] for signal in self.trades],
            mode='markers',
            name="Order_close",
            marker=dict(color="Blue"),
            # hovertext=[
            #     "Position close: {}. Index: {}".format(signal['trade'], signal['index'], signal['type']) for signal in
            #     self.trades
            # ]
            hovertext=[
                "Position close: {}. Index: {}".format(signal['trade'], signal['index']) for signal in
                self.trades
            ]
        )

        order_take = go.Scatter(
            x=[signal['time'] for signal in self.trades],
            y=[signal['take'] for signal in self.trades],
            mode='markers',
            name="Order_Take",
            marker=dict(color="Yellow"),
            # hovertext=[
            #     "Position close: {}. Index: {}".format(signal['trade'], signal['index'], signal['type']) for signal in
            #     self.trades
            # ]
            hovertext=[
                "Position TAKE: {}. Index: {}".format(signal['trade'], signal['index']) for signal in
                self.trades
            ]
        )

        order_stop = go.Scatter(
            x=[signal['time'] for signal in self.trades],
            y=[signal['stop'] for signal in self.trades],
            mode='markers',
            name="Order_Stop",
            marker=dict(color="Orange"),
            # hovertext=[
            #     "Position close: {}. Index: {}".format(signal['trade'], signal['index'], signal['type']) for signal in
            #     self.trades
            # ]
            hovertext=[
                "Position STOP: {}. Index: {}".format(signal['trade'], signal['index']) for signal in
                self.trades
            ]
        )

        self.fig.append_trace(order_open, row=1, col=1)
        self.fig.append_trace(order_close, row=1, col=1)
        self.fig.append_trace(order_take, row=1, col=1)
        self.fig.append_trace(order_stop, row=1, col=1)

    def plot(self):
        # import ipdb
        # ipdb.set_trace()
        self.get_klines()
        self.get_price_channel()
        self.get_signal()
        self.trade_model.get_statistics()
        self.get_stoch()

        self.fig.update_layout(
            title=f"Symbol:{SYMBOL}. Time frame:{INTERVAL}", xaxis_rangeslider_visible=True
        )
        plot(self.fig)
